import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:primeflutterinternship/components/favorites_icon.dart';
import 'package:primeflutterinternship/constants/styles.dart';
import 'package:primeflutterinternship/model/favorites_model.dart';
import 'package:primeflutterinternship/screens/favorite_words.dart';
import 'package:provider/provider.dart';

final _suggestions = <WordPair>[];

class RandomWords extends StatelessWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final favorites = Provider.of<FavoritesModel>(context);
    final _saved = favorites.getAll;

    return Scaffold(
      appBar: AppBar(
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute<void>(
                  builder: (context) {
                    return const FavoritesScreen();
                  },
                ),
              );
            },
            child: const Padding(
              padding: EdgeInsets.all(16.0),
              child: Icon(Icons.list_alt),
            ),
          ),
        ],
        centerTitle: true,
        title: const Text('Provider app'),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          if (i.isOdd) return const Divider();

          final index = i ~/ 2;
          if (index >= _suggestions.length) {
            _suggestions.addAll(generateWordPairs().take(10));
          }
          final alreadySaved = _saved.contains(_suggestions[index]);
          return ListTile(
            title: Text(
              _suggestions[index].asPascalCase,
              style: TextStyles().biggerFont,
            ),
            trailing: FavoritesIcon(alreadySaved: alreadySaved),
            onTap: () {
              if (alreadySaved == true) {
                favorites.remove(_suggestions[index]);
              } else {
                favorites.add(_suggestions[index]);
              }
            },
          );
        },
      ),
    );
  }
}
