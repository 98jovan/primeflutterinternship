import 'package:flutter/material.dart';
import 'package:primeflutterinternship/components/favorites_icon.dart';
import 'package:primeflutterinternship/constants/styles.dart';
import 'package:primeflutterinternship/model/favorites_model.dart';
import 'package:provider/provider.dart';

class FavoritesScreen extends StatelessWidget {
  const FavoritesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final favorites = Provider.of<FavoritesModel>(context);
    final saved = favorites.getAll;

    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: GestureDetector(
              onTap: () {
                favorites.removeAll();
              },
              child: const Icon(Icons.delete),
            ),
          ),
        ],
        title: const Text('List of favorites'),
      ),
      body: ListView.builder(
        itemCount: saved.length,
        itemBuilder: (context, index) {
          final alreadySaved = saved.contains(saved[index]);
          return ListTile(
            title: Text(
              saved[index].asPascalCase,
              style: TextStyles().biggerFont,
            ),
            trailing: FavoritesIcon(alreadySaved: alreadySaved),
            onTap: () {
              if (alreadySaved) favorites.remove(saved[index]);
            },
          );
        },
      ),
    );
  }
}
