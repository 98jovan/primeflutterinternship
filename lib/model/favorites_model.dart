import 'package:english_words/english_words.dart';
import 'package:flutter/cupertino.dart';

class FavoritesModel extends ChangeNotifier {
  final List<WordPair> _favorites = [];

  List<WordPair> get getAll => _favorites;

  void add(WordPair wordPair) {
    _favorites.add(wordPair);
    notifyListeners();
  }

  void remove(WordPair wordPair) {
    _favorites.removeWhere((element) => element == wordPair);
    notifyListeners();
  }

  void removeAll() {
    _favorites.clear();
    notifyListeners();
  }
}
