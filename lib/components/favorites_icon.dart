import 'package:flutter/material.dart';

class FavoritesIcon extends StatelessWidget {
  const FavoritesIcon({
    Key? key,
    required this.alreadySaved,
  }) : super(key: key);

  final bool alreadySaved;

  @override
  Widget build(BuildContext context) {
    return Icon(
      alreadySaved ? Icons.favorite : Icons.favorite_border,
      color: alreadySaved ? Colors.red : null,
    );
  }
}
